/************************************************************************

    name		:	clippy


    description	:	copies actual date to clipboard

    filename	:	main.cpp
    author		:	Sascha Voth
    date		:	2008-07-14

**************************************************************************/

#include <QApplication>
#include <QClipboard>
#include <QDate>

int main (int  argc, char *argv[]) {

    QApplication app(argc, argv);
    QClipboard *clipboard = QApplication::clipboard();

    QString date = QDate::currentDate().toString("yyyy_MM_dd");

    QString combined = date;

    clipboard->setText(combined, QClipboard::Clipboard);

    return 0;

}

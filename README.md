# clippy

Little helper to get the current date (yyyy-MM-dd) in your clipboard.

## Getting Started

Click it.

### Prerequisites

Currently only Windows Build is configured for AppVeyor.

### Installing

Download the artifact folder, and feel free to set shortcuts, everywhere needed.

## Built With

* [Qt](https://www.qt.io/) - Cross-platform software development for embedded and desktop

## Authors

* **Sascha Voth** - *Initial work* - [sascha-voth](https://gitlab.com/sascha-voth/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Attribution

* [SILK ICONS](http://www.famfamfam.com/lab/icons/silk/) - The beatifull date icon is provided by famfamfam.com
* [AppVeyor](https://www.appveyor.com) - Providing free build service for open source projects.